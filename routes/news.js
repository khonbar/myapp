var express = require('express');
require('dotenv').config()
var router = express.Router();
var jwt = require('jsonwebtoken');
const pool = require("../config_db");

/* GET All News */
router.get('/',async function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY,async function(err, decoded) {
    console.log(decoded) // bar
    if(decoded)
    {
      let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * from news n where stt=1");
    res.json(rows);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    }
    else
    {
      res.json({"err":"No JWT"});
    }
    
  });
});
/* GET News by ID  */
router.get('/:id', function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY,async function(err, decoded) {
    if(decoded)
    {
      let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * from news n where stt=1 and id=?",[req.params.id]);
    res.json(rows[0]);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    }
    else
    {
      res.json({"err":"No JWT"});
    }
  });
});
router.get('/cat/:id', function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY,async function(err, decoded) {
    console.log(decoded) // bar
    if(decoded)
    {
      let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * from news n where stt=1 and catid=?",[req.params.id]);
    res.json(rows[0]);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    }
    else
    {
      res.json({"err":"No JWT"});
    }
    
  });
});
router.put('/:id', function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY,async function(err, decoded) {
    console.log(decoded) // bar
    if(decoded)
    {
      let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("UPDATE news set name=?,detail=?,pic=?,file =?,catid=? where id =?",
    [req.body.name,req.body.detail,req.body.pic,req.body.file,req.body.catid,req.params.id]);
    res.json(rows);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    }
    else
    {
      res.json({"err":"No JWT"});
    }
    
  });
});

/* POST ADD News */
router.post('/', function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY,async function(err, decoded) {
    console.log(decoded) // bar
    if(decoded)
    {
        var uid= decoded.id;

      
      let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("INSERT into news (name,detail,pic,file,catid,uid) values(?,?,?,?,?,?);",
    [req.body.name,req.body.detail,req.body.pic,req.body.file,req.body.catid,decoded.id]);
    res.json(rows);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    }
    else
    {
      res.json({"err":"No JWT"});
    }
    
  });
});

router.delete('/:id', function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY,async function(err, decoded) {
    console.log(decoded) // bar
    if(decoded)
    {
      let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("UPDATE news set stt=0 where id =?",
    [req.params.id]);
    res.json(rows);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    }
    else
    {
      res.json({"err":"No JWT"});
    }
    
  });
});


module.exports = router;