var express = require('express');
require('dotenv').config()
var router = express.Router();
var jwt = require('jsonwebtoken');
const pool = require("../config_db");

/* GET All CAT */
router.get('/',async function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY,async function(err, decoded) {
    console.log(decoded) // bar
    if(decoded)
    {
      let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("SELECT * from cat n where stt=1");
    res.json(rows);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    }
    else
    {
      res.json({"err":"No JWT"});
    }
    
  });
});

router.put('/:id', function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY,async function(err, decoded) {
    console.log(decoded) // bar
    if(decoded)
    {
      let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("UPDATE cat set name=? where id =?",
    [req.body.name,req.params.id]);
    res.json(rows);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    }
    else
    {
      res.json({"err":"No JWT"});
    }
    
  });
});

/* POST ADD CAT */
router.post('/', function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY,async function(err, decoded) {
    console.log(decoded) // bar
    if(decoded)
    {
      let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("INSERT into cat (name) values(?);",
    [req.body.name]);
    res.json(rows);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    }
    else
    {
      res.json({"err":"No JWT"});
    }
    
  });
});

router.delete('/:id', function(req, res, next) {
  jwt.verify(req.headers.token, process.env.API_KEY,async function(err, decoded) {
    console.log(decoded) // bar
    if(decoded)
    {
      let conn;
    try {
    conn = await pool.getConnection();
    const rows = await conn.query("UPDATE cat set stt=0 where id =?",
    [req.params.id]);
    res.json(rows);
    } catch (err) {
    res.json(err);
    throw err;
    } finally {
    if (conn) conn.release(); //release to pool
    }
    }
    else
    {
      res.json({"err":"No JWT"});
    }
    
  });
});


module.exports = router;