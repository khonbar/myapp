var createError = require('http-errors');
var express = require('express');
var cors = require('cors')
var path = require('path');
require('dotenv').config();
const pool = require("./config_db");
var logger = require('morgan');
var jwt = require('jsonwebtoken');
var indexRouter = require('./routes/index');

var usersRouter = require('./routes/users');
var newsRouter = require('./routes/news');
var commentsRouter = require('./routes/coments');
var catRouter = require('./routes/cat');
const e = require('express');
var app = express();
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));

//Public API
app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use(async function(req, res, next) {
  var token =req.headers.token;
  if(req.headers.token){
    jwt.verify(req.headers.token, process.env.API_KEY, function(err, decoded) {
      console.log(decoded.iat+ 24*60*60) // bar
    //Check time token
    // console.log(Date.now()/1000 + 24*60*60)
      if(Date.now()/1000 > decoded.iat + 24*60*60)
      {
        res.status(499).json(
          {
          err:"403",
          text:"token in exrip"
          }
        );
        return;
      }
    });
  }
  let conn;
  try {
	conn = await pool.getConnection();
  const rows = await conn.query("SELECT * FROM users where tk = ?",[token]);
  if(rows[0]){
   next()
  }
  else
  {
    res.status(499).json({
      err:"403",
      text:"wrong token"});
    return;
  }
  } catch (err) {
  res.json(err);
  throw err;
  } finally {
	if (conn) conn.release(); //release to pool
  }
});

//Private API with TWT

app.use('/news', newsRouter);
app.use('/comments', commentsRouter);
app.use('/cat', catRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json('error');
});

module.exports = app;
